/*
 * quadrotor.cpp
 *
 *  Created on: Mar 26, 2012
 *      Author: mark
 */

#include "outer_loop/quadrotor.hpp"

extern bool ABORT;
extern int ABORT_cnt;

namespace acl {
namespace outer_loop {

// Initialize Quadrotor Vehicle
Quadrotor::Quadrotor()
{
	// Get vehicle name
	name = ros::this_node::getNamespace();
	// Erase slashes
	name.erase(0,2);
	cntrl_cnt = 0;
	// std::cout<< name << std::endl;
	// Initialize integrators
	I_x = Integrator();
	I_y = Integrator();
	I_z = Integrator();

	// Initialize structs
	gains = new sOuterGains();
	goal = sGoal();
	state = sState();
	calData = sCalibrationData();

	// Initialize variables
	sat = Eigen::Vector3d::Ones();
	yawInitialized = false;
	killMotors_ = false;
        attStatus = DISABLED;
	throttle = 0.0;
	accel_fb_old << 0,0,0;
	jerk_fb_old << 0,0,0;
	vel_old << 0,0,0;
	t_old = 0.0;
	qmin_old.w() = 1.0; qmin_old.vec() << 0,0,0;

	// Default position mode
	xy_mode = POS_CONTROL;
	z_mode = POS_CONTROL;

	// get parameters from param server
	getParams();
}

// Main control loop
void Quadrotor::runController(const ros::TimerEvent& e)
{
	Eigen::Vector3d F = getAccel();
	goal.att = getAttitude(F);
	goal.rate = getRate(F);
	// convert F to motor commands, set throttle
	throttle = F2motorCmd(goal.f_total);
	checkStatus();
	sendCmds();
	if (screen_print_){
		screenPrint();
	}
}

// check the status of the vehicle and set desired values to zero if not good
void Quadrotor::checkStatus()
{
	if (not ABORT)
	{
		shouldFly = true;
		errorMsg.str(""); // clear string
		warnMsg.str(""); // clear string

		if (startTime.now().toSec() <= (startTime.toSec() + spinup_time_))
		{
			goal.rate << 0,0,0;
			I_x.reset();
			I_y.reset();
			I_z.reset();
			if (not yawInitialized)
			{
				goal.yaw = getYaw(state.att); //set goal yaw to initial yaw
				if (goal.yaw != 0.0)
				{
					yawInitialized = true; // this function could be called before the vicon callbacks.  Real yaw data will not be equal to 0
				}
			}
			goal.att.w() = cos(goal.yaw/2); goal.att.vec() << 0,0,sin(goal.yaw/2);
			throttle = 0.2;
		}

		if (state.time == ros::Time(0)) {
			errorMsg << "Not receiving state. Is snap autopilot running and IMU calibration complete?" << std::endl;
			shouldFly = false;
		} else {
			shouldFly = true;
		}

		if (comm_ages_.vicon_age_secs > timeout_limit_) {
			errorMsg << "Not getting external pose measurment. Killing motors." << std::endl;
			killMotors_ = true;
		}

		if (state.pos.z() > alt_limit_) {
			errorMsg << "Altitude limit exceeded. Killing motors." << std::endl;
			killMotors_ = true;
		}

		if (killMotors_) shouldFly = false;

		// If you shouldn't be flying, zero all the commands
		if (not shouldFly or attStatus == DISABLED)
		{
			throttle = 0.0;
			attStatus = DISABLED;
			goal.att.w() = 1;  goal.att.vec() << 0,0,0;
			goal.rate << 0,0,0;
			I_x.reset();
			I_y.reset();
			I_z.reset();
			goal.f_total = 0.0;
			goal.pos << 0,0,0;
			goal.vel << 0,0,0;
			goal.accel << 0,0,0;
			goal.jerk << 0,0,0;
			goal.accel_fb << 0,0,0;
			goal.jerk_fb << 0,0,0;
		}
	}
	else
	{
		ROS_WARN_STREAM("SHUTTING DOWN");
		ABORT_cnt++;
		if (ABORT_cnt > 0)
		{
			sleep(0.5);
			ros::shutdown();
		}
	}
}

// broadcast all pertinent control data as ros messages for logging and debugging
void Quadrotor::sendCmds()
{
	/******** Command Data *********/
	snapstack_msgs::QuadCntrl cmd;
	snapstack_msgs::QuadAttCmd cmd_att;

	// Header
	cmd.header.stamp = ros::Time::now();
	cmd.header.frame_id = name;

	cmd_att.header.stamp = ros::Time::now();
	cmd_att.header.frame_id = name;	

	// Commanded values
	cmd.pose_actual.position.x = state.pos(0);
	cmd.pose_actual.position.y = state.pos(1);
	cmd.pose_actual.position.z = state.pos(2);
	quat2Msg(state.att, cmd.pose_actual.orientation);
	vec2Msg(state.vel, cmd.twist_actual.linear);
	vec2Msg(state.rate, cmd.twist_actual.angular);
	cmd.pose.position.x = goal.pos(0);
	cmd.pose.position.y = goal.pos(1);
	cmd.pose.position.z = goal.pos(2);
	quat2Msg(goal.att, cmd.pose.orientation);
	vec2Msg(goal.vel, cmd.twist.linear);
	vec2Msg(goal.rate, cmd.twist.angular);
	vec2Msg(goal.accel, cmd.accel);
	vec2Msg(goal.accel_fb, cmd.accel_fb);
	vec2Msg(goal.jerk, cmd.jerk);
	vec2Msg(goal.jerk_fb, cmd.jerk_fb);
	cmd.pos_integrator.x = I_x.value;
	cmd.pos_integrator.y = I_y.value;
	cmd.pos_integrator.z = I_z.value;
	// cmd.b = saturate(10*gains->b_hat,1,-1);
	// cmd.a = 0;
	// cmd.u = uf;
	cmd.f_total = goal.f_total;
	cmd.throttle = throttle;
	cmd.att_status = attStatus;
	cmd.vicon_time = ros::Time::now().toSec() - state.time.toSec();

	double r, p, y;
	quaternion2Euler(goal.att, r, p, y);
	cmd.rpy.x = r; // roll
	cmd.rpy.y = p; // pitch
	cmd.rpy.z = y; // heading

	quaternion2Euler(state.att, r, p, y);
	cmd.rpy_actual.x = r; // roll
	cmd.rpy_actual.y = p; // pitch
	cmd.rpy_actual.z = y; // heading
	cmd.yaw = goal.yaw; // yaw

	quat2Msg(goal.att, cmd_att.att);
	vec2Msg(goal.rate, cmd_att.rate);
	cmd_att.throttle = throttle;
	cmd_att.att_status = attStatus;

	cmds_att.publish(cmd_att);
	cmds.publish(cmd);
}

void Quadrotor::screenPrint()
{
	if (not errorMsg.str().empty())
		ROS_ERROR_STREAM_THROTTLE(SCREEN_PRINT_RATE, errorMsg.str());

	if (not warnMsg.str().empty())
		ROS_WARN_STREAM_THROTTLE(SCREEN_PRINT_RATE, warnMsg.str());

	std::ostringstream msg;
	msg.setf(std::ios::fixed); // give all the doubles the same precision
	msg.setf(std::ios::showpos); // show +/- signs always
	msg << std::setprecision(4) << std::endl; // set precision to 4 decimal places
	msg << "Act Pos:  x: " << state.pos(0) << "  y: " << state.pos(1)
			<< "  z: " << state.pos(2) << std::endl;
	msg << "Des Pos:  x: " << goal.pos(0) << "  y: " << goal.pos(1)
			<< "  z: " << goal.pos(2) << std::endl << std::endl;

	msg << "Act Vel:  x: " << state.vel(0) << "  y: " << state.vel(1)
			<< "  z: " << state.vel(2) << std::endl;
	msg << "Des Vel:  x: " << goal.vel(0) << "  y: " << goal.vel(1)
			<< "  z: " << goal.vel(2) << std::endl << std::endl;

	double r, p, y;
	quaternion2Euler(state.att, r, p, y);
	msg << "Act Att:  r: " << r << "  p: " << p << "  y: " << y << std::endl;
	quaternion2Euler(goal.att, r, p, y);
	msg << "Des Att:  r: " << r << "  p: " << p << "  y: " << y << std::endl
			<< std::endl;

	msg << "Act Rate: p: " << state.rate(0) << "  q: " << state.rate(1)
			<< "  r: " << state.rate(2) << std::endl;
	msg << "Des Rate: p: " << goal.rate(0) << "  q: " << goal.rate(1)
			<< "  r: " << goal.rate(2) << std::endl << std::endl;

	msg << "Thrust: " << goal.f_total << "  Throttle: " << throttle
			<< std::endl;
	msg << "AttStatus: " << attStatus << "  XY-Mode: " << xy_mode << "  Z-Mode: " << z_mode << std::endl << std::endl;

	msg << "Seconds since last state update: "
			<< ros::Time::now().toSec() - state.time.toSec() << std::endl;
        msg << "Safety motor kill: " << killMotors_ ;
	msg << std::endl << std::endl;

	ROS_INFO_STREAM_THROTTLE(SCREEN_PRINT_RATE, msg.str());
	// Print at 1/0.5 Hz = 2 Hz
}

void Quadrotor::quat2Msg(Eigen::Quaterniond q, geometry_msgs::Quaternion &q_ros)
{
	q_ros.w = q.w();
	q_ros.x = q.x();
	q_ros.y = q.y();
	q_ros.z = q.z();
}

void Quadrotor::vec2Msg(Eigen::Vector3d v, geometry_msgs::Vector3 &v_ros)
{
	v_ros.x = v(0);
	v_ros.y = v(1);
	v_ros.z = v(2);
}

void Quadrotor::Msg2quat(geometry_msgs::Quaternion q_ros, Eigen::Quaterniond& q)
{
	q.w() = q_ros.w;
	q.vec() << q_ros.x, q_ros.y, q_ros.z;
}

void Quadrotor::Msg2vec(geometry_msgs::Vector3 v_ros, Eigen::Vector3d& v)
{
	v << v_ros.x, v_ros.y, v_ros.z;	
}

void Quadrotor::commagesCallback(const snapstack_msgs::CommAge &msg){
	comm_ages_ = msg;
}

void Quadrotor::stateCallback(const snapstack_msgs::State &msg){

	state.time = ros::Time::now();

	geometry_msgs::Vector3 position;
	position.x = msg.pos.x;
	position.y = msg.pos.y;
	position.z = msg.pos.z;
	Msg2vec(position, state.pos);
	Msg2quat(msg.quat, state.att);

	geometry_msgs::Vector3 vel;
	vel.x = msg.vel.x;
	vel.y = msg.vel.y;
	vel.z = msg.vel.z;

	geometry_msgs::Vector3 rate;
	rate.x = msg.w.x;
	rate.y = msg.w.y;
	rate.z = msg.w.z;

	Msg2vec(vel, state.vel);
	Msg2vec(rate, state.rate);
}

// get goal states
void Quadrotor::goalCallback(const snapstack_msgs::QuadGoal& msg)
{
	Msg2vec(msg.pos, goal.pos);
	Msg2vec(msg.vel, goal.vel);
	Msg2vec(msg.accel, goal.accel);
	Msg2vec(msg.jerk, goal.jerk);
	goal.yaw = msg.yaw;
	goal.dyaw = msg.dyaw;

	// ROS_INFO_STREAM(msg);
	if (msg.cut_power || killMotors_) // disable
	{
		attStatus = DISABLED;
		
		return;
	}

	if (!msg.cut_power && attStatus==DISABLED)
	{
		ROS_INFO_STREAM("Takeoff command received");
		attStatus = FLYING;
		startTime = startTime.now();
	}
	
	if (msg.reset_xy_int) // Reset xy integrators
	{
		I_x.reset();
		I_y.reset();
	}

	if (msg.reset_z_int) // Reset xyz integrators
	{
		I_z.reset();
	}
	if (msg.z_mode == snapstack_msgs::QuadGoal::MODE_POS){
		z_mode = POS_CONTROL;
	}
	else if (msg.z_mode == snapstack_msgs::QuadGoal::MODE_VEL){
		z_mode = VEL_CONTROL;
	}
	else if (msg.z_mode == snapstack_msgs::QuadGoal::MODE_ACCEL){
		z_mode = ACCEL_CONTROL;
	}
	if (msg.xy_mode == snapstack_msgs::QuadGoal::MODE_POS){
		xy_mode = POS_CONTROL;
		// Should reset vel integrators since they're in global frame?
		// I_vx.reset();
		// I_vy.reset();
		// I_vz.reset();
	}
	else if(msg.xy_mode == snapstack_msgs::QuadGoal::MODE_VEL)
	{
		xy_mode = VEL_CONTROL;
		// Should reset pos integrators since they're in global frame?
		// I_x.reset();
		// I_y.reset();
	}
	else if (msg.xy_mode == snapstack_msgs::QuadGoal::MODE_ACCEL){
		xy_mode = ACCEL_CONTROL;
	}
}


//## From Wikipedia - http://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
void Quadrotor::quaternion2Euler(tf2::Quaternion q, double &roll, double &pitch, double &yaw)
{
	tf2::Matrix3x3(q).getRPY(roll, pitch, yaw);
}

void Quadrotor::quaternion2Euler(Eigen::Quaterniond q, double &roll, double &pitch, double &yaw)
{
	tf2::Quaternion tf_q(q.x(),q.y(),q.z(),q.w());
	quaternion2Euler(tf_q,roll,pitch,yaw);
}

void Quadrotor::quaternion2Euler(geometry_msgs::Quaternion q, double &roll, double &pitch, double &yaw)
{
	tf2::Quaternion tf_q(q.x,q.y,q.z,q.w);
	quaternion2Euler(tf_q,roll,pitch,yaw);
}

Eigen::Vector3d Quadrotor::saturate(Eigen::Vector3d val, Eigen::Vector3d max, Eigen::Vector3d min)
{
	for (int ii=0;ii<3;ii++){
		val(ii) = saturate(val(ii),max(ii),min(ii));
	}
	return val;
}

/**
 * Saturates the input value
 * @param val Input value
 * @param max Maximum value
 * @param min Minimum value
 */
double Quadrotor::saturate(double val, double max, double min)
{
    if (val > max)
        val = max;
    else if (val < min)
        val = min;

    return val;
}

double Quadrotor::getYaw(Eigen::Quaterniond q){
	return atan2(2*(q.w()*q.z()+q.x()*q.y()),1-2*(q.y()*q.y()+q.z()*q.z()));
}

Eigen::Quaterniond Quadrotor::checkQuat(Eigen::Quaterniond qmin, Eigen::Quaterniond old, Eigen::Quaterniond state)
{
	// check for continuity with last qmin
	if (qmin.dot(old) < 0.0)
	{
		qmin.w() = -qmin.w();
		qmin.vec() = -qmin.vec();
	}

	// check for continuity with state quaternion
	if (qmin.dot(state) < 0.0)
	{
		qmin.w() = -qmin.w();
		qmin.vec() = -qmin.vec();
	}
	qmin.normalize();

	return qmin;
}

void Quadrotor::qnorm(Eigen::Quaterniond &q)
{
	if (q.norm() < 0.001)
		ROS_WARN("Can't normalize quaternion -- length == 0.0");
	else
		q.normalize();
}

void Quadrotor::vnorm(Eigen::Vector3d &v)
{
	if (v.norm() < 0.001)
		ROS_WARN("Can't normalize vector -- length == 0.0");
	else
		v.normalize();
}


void Quadrotor::getParams()
{
	ros::param::get("~spinup_time",spinup_time_);
	ros::param::get("~mass",mass);
	ros::param::get("~pose_timeout",timeout_limit_);
	ros::param::get("~alt_limit",alt_limit_);
	ros::param::get("~screenPrint",screen_print_);

	double kp_xy; ros::param::get("~Kp/xy",kp_xy);
	double ki_xy; ros::param::get("~Ki/xy",ki_xy);
	double ki_vxy; ros::param::get("~Ki/vxy",ki_vxy);
	double kd_xy; ros::param::get("~Kd/xy",kd_xy);
	double kp_z; ros::param::get("~Kp/z",kp_z);
	double ki_z; ros::param::get("~Ki/z",ki_z);
	double kd_z; ros::param::get("~Kd/z",kd_z);
	double maxposerr_xy;ros::param::get("~maxPosErr/xy",maxposerr_xy);
	double maxposerr_z ;ros::param::get("~maxPosErr/z",maxposerr_z);
	double maxvelerr_xy;ros::param::get("~maxVelErr/xy",maxvelerr_xy);
	double maxvelerr_z ;ros::param::get("~maxVelErr/z",maxvelerr_z);
	gains->Kp(0) = (kp_xy);	
	gains->Kp(1) = (kp_xy);
	gains->Ki(0) = (ki_xy);
	gains->Ki(1) = (ki_xy);
	gains->Kd(0) = (kd_xy);
	gains->Kd(1) = (kd_xy);
	gains->Kp(2) = (kp_z);
	gains->Ki(2) = (ki_z);
	gains->Kd(2) = (kd_z);
	gains->maxPosErr(0) = (maxposerr_xy);
	gains->maxPosErr(1) = (maxposerr_xy);
	gains->maxPosErr(2) = (maxposerr_z);
	gains->maxVelErr(0) = (maxvelerr_xy);
	gains->maxVelErr(1) = (maxvelerr_xy);
	gains->maxVelErr(2) = (maxvelerr_z);
	ros::param::get("~minRmag",gains->minRmag);
	ros::param::get("~minRmagint",gains->minRmagint);
	ros::param::get("~f2cmd/a",gains->a);
	ros::param::get("~f2cmd/b",gains->b);
	ros::param::get("~f2cmd/c",gains->c);
}

} // ns outer_loop
} // ns acl
