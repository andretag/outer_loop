/*
 * main.cpp
 *
 *  Created on: Mar 26, 2011
 *      Author: mark
 */

#include <iostream>
#include <csignal>

#include "outer_loop/quadrotor.hpp"
#include "outer_loop/defines.h"

// Global vars
bool ABORT = false;
int ABORT_cnt = -10;

// Prototypes
void controlC(int sig);

int main(int argc, char **argv)
{
    // Initialize ROS
    // supplied name is overwritten by name remapping from command line
    ros::init(argc, argv, "cntrl", ros::init_options::NoSigintHandler);
    ros::NodeHandle nh("");
    ros::NodeHandle nhp("~");

    // Check for proper renaming of node
    if (!ros::this_node::getNamespace().compare("/"))
    {
        ROS_ERROR("Error :: You should be using a launch file to specify the node namespace!\n");
        return -1;
    }

    // Initialize vehicle
    using namespace acl::outer_loop;
    Quadrotor quad;

    // initialize listener callbacks that pickup the vicon data
    ros::Subscriber sub_state = nh.subscribe("state", 1, &Quadrotor::stateCallback, &quad);
    // initialize main controller loop
    ros::Timer controllerTimer = nhp.createTimer(ros::Duration(controlDT), &Quadrotor::runController, &quad);
    // initialize listener callback that subscribes to goal data
    ros::Subscriber sub_goal = nh.subscribe("goal", 1, &Quadrotor::goalCallback, &quad);
    // initialize listener callback that subscribes to comm_ages data
    ros::Subscriber sub_comm = nh.subscribe("comm_ages", 1, &Quadrotor::commagesCallback, &quad);
    // publish controller and goal commands
    quad.cmds = nh.advertise<snapstack_msgs::QuadCntrl>("cmds", 1);
    // publish controller and goal commands
    quad.cmds_att = nh.advertise<snapstack_msgs::QuadAttCmd>("attCmd", 1);
    
    /* Setup the CTRL-C trap */
    signal(SIGINT, controlC);
    sleep(0.5);

    // run the code
    // start asynchronous spinner
    ros::AsyncSpinner spinner(4); // Use 4 threads
    spinner.start();
    ros::waitForShutdown();
    return 0;
}

//## Custom Control-C handler
void controlC(int sig)
{
    ABORT = true; // ros::shutdown called in Quadrotor::sendCmd() function
}
