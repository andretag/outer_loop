/*
 * control.cpp
 *
 *  Created on: May 30, 2012
 *      Author: brett
 */
#include "outer_loop/quadrotor.hpp"

namespace acl {
namespace outer_loop {

Eigen::Vector3d Quadrotor::getAccel()
{
    // Position Errors
    Eigen::Vector3d pos_err = saturate(goal.pos - state.pos, gains->maxPosErr, -gains->maxPosErr);

    // Velocity Errors
    Eigen::Vector3d vel_err = saturate(goal.vel - state.vel, gains->maxVelErr, -gains->maxVelErr);


    if (xy_mode==POS_CONTROL){
        if (fabs(pos_err(0)) < gains->maxPosErr(0) && this->attStatus)
        {
            I_x.increment(pos_err(0), controlDT);
        }
        if (fabs(pos_err(1)) < gains->maxPosErr(1) && this->attStatus)
        {
            I_y.increment(pos_err(1), controlDT);
        }
    }

    if (z_mode == POS_CONTROL){
        if (fabs(pos_err(2)) < gains->maxPosErr(2) && this->attStatus)
        {
            I_z.increment(pos_err(2), controlDT);
        }
    }

    if (xy_mode==ACCEL_CONTROL){
        pos_err(0)=0;
        pos_err(1)=0;
        vel_err(0)=0;
        vel_err(1)=0;
    }

    
    if (z_mode==ACCEL_CONTROL){
        pos_err(2)=0;
        vel_err(2)=0;
    }

    Eigen::Vector3d int_err(I_x.value, I_y.value, I_z.value);

    goal.accel_fb = gains->Kp.cwiseProduct(pos_err) + gains->Kd.cwiseProduct(vel_err) + gains->Ki.cwiseProduct(int_err);

    Eigen::Vector3d r_nom; // ignore feedback here
    r_nom(0)=(mass * goal.accel(0));
    r_nom(1)=(mass * goal.accel(1));
    r_nom(2)=(mass * (goal.accel(2) + GRAVITY));
    gains->rmag = r_nom.norm();

    // Numerically Differentiate feedback acceleration
    double dt = ros::Time::now().toSec() - t_old;
    t_old = ros::Time::now().toSec();
    if (dt > 0.0)
    {
        goal.jerk_fb = (goal.accel_fb - accel_fb_old) / dt;

        // filter the differentiation
        double alpha = dt / (0.1 + dt);
        goal.jerk_fb = jerk_fb_old + alpha * (goal.jerk_fb - jerk_fb_old);

    }

    // update variables
    accel_fb_old = goal.accel_fb;
    jerk_fb_old = goal.jerk_fb;


    Eigen::Vector3d F;
    F(0) = mass * (goal.accel(0) + goal.accel_fb(0));
    F(1) = mass * (goal.accel(1) + goal.accel_fb(1));
    F(2) = mass * (goal.accel(2) + goal.accel_fb(2) + GRAVITY);

    return F;
}

Eigen::Quaterniond Quadrotor::getAttitude(Eigen::Vector3d F)
{
    Eigen::Vector3d Fbar = F;
    Fbar.normalize();
    // pure quaternion (0 scalar part)
    Eigen::Quaterniond qFbar(0.0, Fbar(0), Fbar(1), Fbar(2));
    Eigen::Vector3d Fbarb, FbxF;
    Fbarb << 0.0, 0.0, 1.0;
    FbxF = Fbarb.cross(Fbar);
    double FbdF = Fbarb.dot(Fbar);
    Eigen::Quaterniond qmin(1,0,0,0);

    double eps = 0.05;
    if (gains->rmag > gains->minRmag)
    {
        if (FbdF + 1 > eps)
        {
            double tmp = 1.0 / sqrt(2 * (1 + FbdF));
            qmin.w() = tmp * (1 + FbdF);
            qmin.vec() << tmp * FbxF(0),  tmp * FbxF(1), tmp * FbxF(2) ;
        }
    }
    qmin.normalize();

    // recompute r3 using true desired acceleration
    F(2) = mass * (goal.accel_fb(2) + GRAVITY);
    
    // Correct sign ambiguity if desired z acceleration is greater than gravity
      if (F(2) < 0){
        F(2) = 0;
      }

    goal.f_total = F.norm();

    // only assign goal attitude if you aren't close to a singularity
    Eigen::Quaterniond att_des;
    if (gains->rmag > gains->minRmag)
    {
        // ensure quaternions agree with each other
        qmin = checkQuat(qmin, qmin_old, state.att);
        qmin_old = qmin;

        att_des = qmin;

        // Rotate by yaw
        tf2::Quaternion qyaw_des;
        qyaw_des.setRPY(0.0, 0.0, goal.yaw);
        Eigen::Quaterniond qyaw(qyaw_des.w(),qyaw_des.x(),qyaw_des.y(),qyaw_des.z());
        att_des *= qyaw;
    }
    else {
        att_des = qmin_old;
    }
    return att_des;
}


Eigen::Vector3d Quadrotor::getRate(Eigen::Vector3d F)
{
    Eigen::Vector3d rate;

    // Compute desired rates - see Lopez SM for derivation
    Eigen::Vector3d Fdot, Fbardot, Fbar, omega;
    Fbar = F / F.norm();
    Fdot = mass * (goal.jerk + goal.jerk_fb);
    Fbardot = Fdot / F.norm() - F * F.dot(Fdot) / pow(F.norm(), 3);
    // omega = Fbar.cross(Fbardot);

    Eigen::Vector3d temp = goal.att.conjugate()._transformVector(Fbardot);
    omega << -temp(1),temp(0),temp(2);

    if (gains->rmag > gains->minRmag)
    {
        rate = omega;
    }

    double r, p, y;
    quaternion2Euler(goal.att, r, p, y);
    
    // yaw rate is specified as an input in world frame, need to rotate it to body frame
    rate(2) = cos(r)*cos(p)*goal.dyaw;

    return rate;
}

// convert a desired force in newtons to desired motor command
double Quadrotor::F2motorCmd(double f)
{
    double throttle = gains->a * f * f + gains->b * f + gains->c;
    return saturate(throttle, 0.9, 0.0);
}

} // ns outer_loop
} // ns acl
