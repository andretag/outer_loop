/*
 * structs.h
 *
 *  Created on: Aug 9, 2011
 *      Author: mark
 */

#ifndef STRUCTS_H_
#define STRUCTS_H_

#include <vector>
#include <cstdint>

#include <Eigen/Dense>

#include <geometry_msgs/Vector3.h>

//********************* CONTROL STRUCTS **********************//
struct sGoal
{
  Eigen::Quaterniond att;           // Attitude
  Eigen::Vector3d rate;           // Angular rates
  Eigen::Vector3d pos, vel, accel, jerk;    // Position, velocity, accleration and jerk
  Eigen::Vector3d accel_fb, jerk_fb;      // Feedback modified acceleration and jerk
  double f_total;             // Thrust in newtons
  double yaw, dyaw, heading, dheading;            // Yaw angle, yaw rate, heading angle, heading rate

  // Constructor
  sGoal()
  {
    att.w() = 1.0;
    att.vec() << 0,0,0;
    rate  << 0,0,0;
    pos   << 0,0,0;
    vel   << 0,0,0;
    accel << 0,0,0;
    jerk  << 0,0,0;
    accel_fb  << 0,0,0;
    jerk_fb   << 0,0,0;
    f_total = yaw = dyaw = heading = dheading = 0.0;
  }
  ;
};

struct sState
{
  Eigen::Quaterniond att;         // Attitude
  Eigen::Vector3d pos, vel, accel, rate;  // Position, velocity, acceleration, rate
  ros::Time time; // last time we received information

  // Constructor
  sState()
  {
    att.w() = 1.0;
    att.vec() << 0,0,0;
    pos << 0,0,0;
    vel << 0,0,0;
    accel << 0,0,0;
    rate << 0,0,0;
    time.fromNSec(0);
  }
};

struct sOuterGains
{

  Eigen::Vector3d Kp, Kd, Ki; // Position PID gains
  Eigen::Vector3d maxPosErr;  // maximum position error allowed
  Eigen::Vector3d maxVelErr;  // maximum velocity error allowed
  double I_xy_int_thresh; // threshold on integrating position error
  double I_z_int_thresh;  // threshold on integrating position error

  double rmag;    // acceleration magnitude
  double minRmag;   // minimum allowable acceleration magnitude for calculating attitude
  double minRmagint;  // minimum acceleration magnitude for listening to integrated values

  double a, b, c;     // quadratic terms for calculating motor term from thrust in Newtons

  // Constructor - initialize all values to zero
  sOuterGains()
  {
    Kp  << 0,0,0;
    Ki  << 0,0,0;
    Kd  << 0,0,0;
    maxPosErr << 0,0,0;
    maxVelErr << 0,0,0;
    I_xy_int_thresh = I_z_int_thresh = 0.0;
    rmag = minRmag = minRmagint = 0.0;
    a = b = c = 0.0;
  }
  ;
};

struct sInnerGains
{
  Eigen::Vector3d L, K, P; // x -> roll, y -> pitch, z - > yaw
  double lowBatt;

  // Constructor:
  sInnerGains()
  {
    L << 0,0,0;
    K << 0,0,0;
    P << 0,0,0;
    lowBatt = 0.0;
  }
  ;
};

struct sModel
{
  Eigen::Vector3d J; // x -> roll, y -> pitch, z - > yaw
  double b, li, ci;
  double lowBatt;

  // Constructor:
  sModel()
  {
    J << 0,0,0;
    b = li = ci = 0;
  }
  ;
};

struct sCalibrationData
{
  double gyroScale;
  double accelScale;
  double K_Att_Filter;
  double kGyroBias;
  double kAccelFilter;

  // Constructor:
  sCalibrationData()
  {
    gyroScale = 0.0;
    accelScale = 0.0;
    K_Att_Filter = 0.0;
    kGyroBias = 0.0;
    kAccelFilter = 0.0;
  }
  ;
};

// FLAGS ############
struct sFlags
{
  bool screenPrint; // print information to screen
  bool attitude_mode;   // directly send attitude and thrust commands
  bool streamData; // Uberpilot streams on-board data
  bool debug; //Debug mode for uberpilot

  // Constructor:
  sFlags()
  {
    streamData = false;
    screenPrint = true;
    attitude_mode = false;
    debug = false;
  }
  ;
};
//************************************************************//

// Integrator
struct Integrator
{
  double value;

  // Constructors:
  Integrator()
  {
    value = 0;
  }
  ;

  // Methods
  void increment(double inc, double dt)
  {
    value += inc * dt;
  }
  void reset()
  {
    value = 0;
  }

};

#endif /* STRUCTS_H_ */
