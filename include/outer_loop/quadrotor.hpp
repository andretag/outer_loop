/*
 * quadrotor.hpp
 *
 *  Created on: Mar 26, 2012
 *      Author: mark
 */

#ifndef QUADROTOR_HPP_
#define QUADROTOR_HPP_

#include <iostream>
#include <fstream>
#include <pthread.h>
#include <sys/types.h>
#include <dirent.h>
#include <stdio.h>
#include <math.h>
#include <vector>
#include <map>
#include <algorithm>
#include <boost/foreach.hpp>
#include <signal.h>

#include <ros/ros.h>

#include <Eigen/Dense>

#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/Vector3Stamped.h>
#include <geometry_msgs/PoseArray.h>
#include <geometry_msgs/Point.h>

#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Vector3.h>

#include <std_msgs/Float64.h>
#include <std_msgs/MultiArrayDimension.h>
#include <std_msgs/MultiArrayLayout.h>
#include <std_msgs/Float64MultiArray.h>
#include <std_srvs/Empty.h>

#include <tf2_geometry_msgs/tf2_geometry_msgs.h>

#include <snapstack_msgs/QuadCntrl.h>
#include <snapstack_msgs/QuadAttCmd.h>
#include <snapstack_msgs/QuadGoal.h>
#include <snapstack_msgs/QuadMode.h>
#include <snapstack_msgs/State.h>
#include <snapstack_msgs/CommAge.h>

#include "outer_loop/structs.h"
#include "outer_loop/defines.h"

namespace acl {
namespace outer_loop {

class Quadrotor
{
public:
    Quadrotor();

    //## ROS message publishing
    ros::Publisher cmds, health, sensorPub, cmds_att;

    // message callbacks
    // void modeCallback(const snapstack_msgs::QuadMode& msg);
    void stateCallback(const snapstack_msgs::State &msg);
    void commagesCallback(const snapstack_msgs::CommAge &msg);
    void goalCallback(const snapstack_msgs::QuadGoal& msg);
    void emergencyLandCallback(const snapstack_msgs::QuadGoal& msg);
    void pathPlannerCallback(const snapstack_msgs::QuadGoal& msg);

    // ROS timed functions
    void runController(const ros::TimerEvent&);

private:

    std::string name; // name of the node (quadrotor that is flying)

    //## Control functions
    Eigen::Vector3d getAccel();
    Eigen::Quaterniond getAttitude(Eigen::Vector3d F);
    Eigen::Vector3d getRate(Eigen::Vector3d F);
    void checkStatus();
    double F2motorCmd(double f);

    //## Send commands function
    void sendCmds();

    //## Logging and Debugging Functions
    void screenPrint();

    //## Parameter functions for accessing param server
    void getParams();

    //## Control Structs and msgs
    sGoal goal;
    sState state;
    sOuterGains* gains;
    sCalibrationData calData;
    Integrator I_x, I_y, I_z;

    snapstack_msgs::CommAge comm_ages_;

    // control information
    double throttle;
    bool yawInitialized, killMotors_;
    int attStatus;
    int cntrl_cnt;
    int xy_mode; // Consider making message
    int z_mode;
    std::ostringstream errorMsg, warnMsg;

    // timer for initializing motors
    ros::Time startTime;

    // Quadrotor Mass
    double mass;
    double spinup_time_, timeout_limit_, alt_limit_;
    bool screen_print_, adapt, casmc, shouldFly;
    // for numerical differentiation of acceleration
    double t_old;
    Eigen::Vector3d accel_fb_old, jerk_fb_old, vel_old, s;
    Eigen::Quaterniond qmin_old;

    Eigen::Vector3d sat;

    // Utilities
    void quaternion2Euler(tf2::Quaternion q, double &roll, double &pitch, double &yaw);
    void quaternion2Euler(Eigen::Quaterniond q, double &roll, double &pitch, double &yaw);
    void quaternion2Euler(geometry_msgs::Quaternion q, double &roll, double &pitch, double &yaw);
    double getYaw(Eigen::Quaterniond q);
    Eigen::Vector3d saturate(Eigen::Vector3d val, Eigen::Vector3d max, Eigen::Vector3d min);
    double saturate(double val, double max, double min);
    Eigen::Quaterniond checkQuat(Eigen::Quaterniond qmin, Eigen::Quaterniond old, Eigen::Quaterniond state);
    void qnorm(Eigen::Quaterniond &q);
    void vnorm(Eigen::Vector3d &q);
    void quat2Msg(Eigen::Quaterniond q, geometry_msgs::Quaternion &q_ros);
    void vec2Msg(Eigen::Vector3d v, geometry_msgs::Vector3 &v_ros);
    void Msg2quat(geometry_msgs::Quaternion q_ros, Eigen::Quaterniond& q);
    void Msg2vec(geometry_msgs::Vector3 v_ros, Eigen::Vector3d& v);

};

} // ns outer_loop
} // ns acl

#endif /* QUADROTOR_HPP_ */
