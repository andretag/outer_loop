/*
 * defines.h
 *
 *  Created on: Mar 26, 2012
 *      Author: mark
 *      Modified: brett
 */

#ifndef DEFINES_H_
#define DEFINES_H_


// Common defines
#ifndef PI
#define PI 3.14159265359 ///< The number PI
#endif
#ifndef GRAVITY
#define GRAVITY 9.81 ///< Gravity constant
#endif

//Defines
#define controlDT               0.01    // 1/100 Control loop rate [seconds]
// Safety values
#define MAX_VICON_WAIT_TIME     0.5 // wait time in seconds for new vicon data
#define MAX_ACCEL_XY            2.0
#define MAX_ACCEL_Z             0.8

#define SCREEN_PRINT_RATE   0.5 // Print at 1/0.5 Hz = 2 Hz

// Waypoint tracking type
#define POS_CONTROL    0
#define VEL_CONTROL    1
#define ACCEL_CONTROL  2

// Flight Mode
#define DISABLED        0
#define FLYING          1

//## communications defines -- must be the same values on embedded code
#define QUAT_SCALE      10000.0     // value to scale quaternions by for sending as int
#define ROT_SCALE       100.0       // value to scale rotations (p,q,r) by for sending as int
#define GAINS_SCALE     10.0        // value to scale gains by for sending as int

#endif /* DEFINES_H_ */
